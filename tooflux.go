package tooflux
//
// Wrapper de l'API client InfluxDB v2
//
import (
	"os"
	"fmt"
	"time"
	"github.com/influxdata/influxdb/client/v2"
)

const (
	// MaxPoints is lower batching limit for measure
	MaxPoints = 1000
)
// TooFlux is used to keep track of InfluxDB connection informations.
// Mach, Port and Base are required.
type TooFlux struct {
	// Mach	address or DNS name of InfluxDB server.
	Mach	string

	// Port port of InfluxDB server at 'Mach' (generally 8086).
	Port	string

	// Base name of InfluxDB database to use.
	Base	string

	// reten is retention policies linked with the base.
	reten	map[string]string

	// clnt is InfluDB client.
	clnt	client.Client

	// nbpts is the number of points stored at a time.
	nbpts	int

	// debug is the debug output indicator.
	debug	bool
}

// NewTooFlux will instanciate and return a connected client or an error.
// NewTooFlux will automatically defer client connection close.
func NewTooFlux( m string, p string, b string, debug bool ) (*TooFlux, error) {
	tf := new(TooFlux)
	tf.Mach = m
	tf.Port = p
	tf.Base = b
	tf.debug = debug

	clnt, err :=
		client.NewHTTPClient(client.HTTPConfig{ Addr: "http://"+m+":"+p } )

	tf.clnt = clnt
	defer tf.clnt.Close()

	if debug && err != nil {
		fmt.Fprintf( os.Stderr, "NewTooFlux(%s,%s,%s): %s.\n", m, p, b, err )
	}
	return tf, err
}

// QueryDB will send query to tf.Base.
func (tf *TooFlux)QueryDB( q client.Query ) (*client.Response, error) {
	response, err := tf.clnt.Query( q )
	if tf.debug {
		if err != nil {
			fmt.Fprintf( os.Stderr, "QueryDB error: %s.\n", err )
		} else if response.Error() != nil {
			fmt.Fprintf( os.Stderr, "QueryDB response error: %s.\n", response.Error() )
		} else {
			fmt.Fprintf( os.Stderr, "QueryDB response: %v.\n", response.Results )
		}
	}
	if err != nil {
		return nil, err
	} else if response.Error() != nil {
		return nil, response.Error()
	} else {
		return response, nil
	}
}

// CreateDB is used to create a new database. If database already exists,
// no error will return.
func (tf *TooFlux)CreateDB() error {
	q := client.NewQuery( fmt.Sprintf( "CREATE DATABASE %s", tf.Base ), "", "" )
	response, err := tf.clnt.Query( q )
	if err != nil {
			return err
	}
	if tf.debug {
		fmt.Fprintf( os.Stderr,
			"CREATE DATABASE %s: %s.\n", tf.Base, response.Results )
	}
	return nil
}

// ExistsDB is used to find if 'dbname' database already exists on server.
func (tf *TooFlux)ExistDB(dbname string) (bool,error) {
	q := client.NewQuery( "show databases", "", "" )
	response, err := tf.clnt.Query( q )
	if err != nil {
		return false, err
	}
	if tf.debug {
		fmt.Fprintf( os.Stderr, "show databases: %v\n", response )
	}

	for _, dbs := range response.Results[0].Series[0].Values {
		for _, db := range dbs {
			if str, ok := db.(string); ok {
				if str == dbname {
					return true, nil
				}
			}
		}
	}

	return false, nil
}

// SetRetentionPolicy creates an InfluDB database retention policy 
func (tf *TooFlux)SetRetentionPolicy( ret_name string, duration string, defaut bool ) error {
	var cmd string

	if tf.debug {
		fmt.Fprintf( os.Stderr, "SetRetentionPolicy(%s,%s,%v)\n",
						ret_name, duration, defaut )
	}
	cmd = fmt.Sprintf( "DURATION %s REPLICATION 1", duration )
	if defaut {
		cmd += " DEFAULT"
	}
	if tf.reten == nil {
		tf.reten = make(map[string]string)
	}
	tf.reten[ret_name]=cmd

	q := client.NewQuery(
			fmt.Sprintf( "CREATE RETENTION POLICY \"%s\" ON \"%s\" %s",
													ret_name, tf.Base, cmd),
			"", "" )
	response, err := tf.clnt.Query( q )
	if err != nil || response.Error() != nil {
		if err != nil {
			return err
		} else {
			return response.Error()
		}
	}

	return nil
}

// SetRetention alters InfluxDB database retention policy.
func (tf *TooFlux)SetRetention( ret_name string ) error {
	q := client.NewQuery(
		fmt.Sprintf( "ALTER RETENTION POLICY \"%s\" ON \"%s\" %s",
			ret_name,tf.Base,tf.reten[ret_name]), "", "" )
	response, err := tf.clnt.Query( q )
	if err != nil && response.Error() != nil {
		if err != nil {
			return err
		} else {
			return response.Error()
		}
	}

	return nil
}

// NewBatchPoints creates an interface to batch group of points before write them.
// NewBatchPoints is not thread safe
func (tf *TooFlux)NewBatchPoints() (client.BatchPoints) {
	bp, _ := client.NewBatchPoints(client.BatchPointsConfig{
		Database:  tf.Base,
		Precision: "us",
	})
	return bp
}

// NewBatchPoints creates an interface to batch group of points before write them.
// NewBatchPoints is not thread safe
func (tf *TooFlux)NewBatchPointsWithPrecision( prec string ) (client.BatchPoints) {
	bp, _ := client.NewBatchPoints(client.BatchPointsConfig{
		Database:  tf.Base,
		Precision: prec,
	})
	return bp
}

// NewBatchPointsWithRetention creates an interface to batch group of points
// before write them under a given retention policy in database.
// NewBatchPointsWithRetention is not thread safe.
func (tf *TooFlux)NewBatchPointsWithRetention( reten string ) (client.BatchPoints) {
	bp, _ := client.NewBatchPoints(client.BatchPointsConfig{
		Database:  tf.Base,
		Precision: "us",
		RetentionPolicy: reten,
	})
	return bp
}

// AddMeasure adds a measurement to Batchpoints.
func (tf *TooFlux)AddMeasure( bp	client.BatchPoints,
							name	string,
							tags	map[string]string,
							fields	map[string]interface{},
							t time.Time ) (*client.Point,error) {
	pt, err := client.NewPoint(
		name,
		tags,
		fields,
		time.Now(),
	)
	if err == nil {
		bp.AddPoint(pt)
		tf.nbpts++;
	}
	if tf.debug {
		fmt.Fprintf( os.Stderr, "AddMeasure(%s,%v,%v): %d.\n",
					name, tags, fields, tf.nbpts )
	}

	return pt, err
}

// sendMeasures send measures when number of points is greater than MaxPoints.
// sending may be forced given that at least one point exists.
func (tf *TooFlux)SendMeasures( db client.BatchPoints, forced bool ) error {
	if tf.nbpts > 0 && (tf.nbpts > MaxPoints || forced) {
		err := tf.clnt.Write( db )
		if tf.debug {
			if err != nil {
				fmt.Fprintf( os.Stderr, "sendMeasures: %s\n", err )
			} else {
				fmt.Fprintf( os.Stderr, "sendMeasures: No error.\n" )
			}
		}
		if err != nil {
			return err
		}
		tf.nbpts = 0
	}

	return nil
}
